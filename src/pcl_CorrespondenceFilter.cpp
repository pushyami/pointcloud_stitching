#include "Pcl_CorrespondenceFilter.h"

using namespace pcl;
using namespace std;

void Pcl_CorrespondenceFilter::filterCorrespondences(const CorrespondencesPtr &all_correspondences, const KeyPointPtr &keypoints_src, const KeyPointPtr &keypoints_tgt,int type = 0)
{
    PointCloud<PointXYZ>::Ptr keyPointsXYZsrc(new PointCloud<PointXYZ>());
    PointCloud<PointXYZ>::Ptr keyPointsXYZdst(new PointCloud<PointXYZ>());
    copyPointCloud(*keypoints_src, *keyPointsXYZsrc);
    copyPointCloud(*keypoints_tgt, *keyPointsXYZdst);

    filteredCorrespondence = CorrespondencesPtr(new Correspondences);


    if (type == 0)
    {
        registration::CorrespondenceRejectorSampleConsensus<PointXYZ> rejectorSC;
        rejectorSC.setInputSource(keyPointsXYZsrc);
        rejectorSC.setInputTarget(keyPointsXYZdst);
        rejectorSC.setInlierThreshold(threshold);
        rejectorSC.setInputCorrespondences(all_correspondences);
        rejectorSC.getCorrespondences(*filteredCorrespondence);

    }
    else if (type == 1)
    {
        registration::CorrespondenceRejectorMedianDistance rejectorMD;
        rejectorMD.setInputSource<PointXYZ>(keyPointsXYZsrc);
        rejectorMD.setInputTarget<PointXYZ>(keyPointsXYZdst);
        rejectorMD.setInputCorrespondences(all_correspondences);
        rejectorMD.setMedianFactor(threshold);
        rejectorMD.getCorrespondences(*filteredCorrespondence);
    }
    else if (type == 2)
    {
        registration::CorrespondenceRejectorDistance rejectorD;
        rejectorD.setInputCloud<PointXYZ>(keyPointsXYZsrc);
        rejectorD.setInputTarget<PointXYZ>(keyPointsXYZdst);
        rejectorD.setMaximumDistance(threshold);
        rejectorD.setInputCorrespondences(all_correspondences);
        rejectorD.getCorrespondences(*filteredCorrespondence);
    }

}

void Pcl_CorrespondenceFilter::filterCorrespondences(vector<int> &correspondences1, vector<int> &correspondences2, KeyPointPtr keyPointsSrc, KeyPointPtr keyPointsDst,int type = 0)
{
    vector<pair<unsigned, unsigned> > correspondences;
    CorrespondencesPtr correspondencePtr(new Correspondences);
    for (unsigned i = 0; i < correspondences1.size(); ++i)
        if (correspondences2[correspondences1[i]] == i)
            correspondences.push_back(std::make_pair(i, correspondences1[i]));

    correspondencePtr->resize(correspondences.size());
    for (unsigned i = 0; i < correspondences.size(); ++i)
    {
        (*correspondencePtr)[i].index_query = correspondences[i].first;
        (*correspondencePtr)[i].index_match = correspondences[i].second;
    }

    filterCorrespondences(correspondencePtr, keyPointsSrc, keyPointsDst,type);
}

