#include "Pcl_Reconstruction.h"

using namespace pcl;
using namespace boost;
using namespace std;

Pcl_Reconstruction::Pcl_Reconstruction()
{
}


Pcl_Reconstruction::~Pcl_Reconstruction()
{
}


PolygonMesh Pcl_Reconstruction::greedyTriangulation(PointCloud<PointXYZ>::Ptr cloudIn){

    // Normal estimation*
    NormalEstimation<PointXYZ, Normal> n;
    PointCloud<Normal>::Ptr normals(new PointCloud<Normal>);
    pcl::search::KdTree<PointXYZ>::Ptr tree(new pcl::search::KdTree<PointXYZ>);
    tree->setInputCloud(cloudIn);
    n.setInputCloud(cloudIn);
    n.setSearchMethod(tree);
    n.setKSearch(10);
    n.compute(*normals);

    //* normals should not contain the point normals + surface curvatures
    // Concatenate the XYZ and normal fields*
    PointCloud<PointNormal>::Ptr cloud_with_normals(new PointCloud<PointNormal>);
    concatenateFields(*cloudIn, *normals, *cloud_with_normals);
    //* cloud_with_normals = cloud + normals

    // Create search tree*
    pcl::search::KdTree<PointNormal>::Ptr tree2(new pcl::search::KdTree<PointNormal>);
    tree2->setInputCloud(cloud_with_normals);
    // Initialize objects
    pcl::GreedyProjectionTriangulation<PointNormal> gp3;
    pcl::PolygonMesh triangles;
    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius(0.045);
    // Set typical values for the parameters
    gp3.setMu(1.5);
    gp3.setMaximumNearestNeighbors(100);
    gp3.setMaximumSurfaceAngle(M_PI / 2); // 180 degrees
    gp3.setMinimumAngle(M_PI / 36); // 5 degrees
    gp3.setMaximumAngle(2 * M_PI); // 180 degrees
    gp3.setNormalConsistency(false);

    // Get result
    gp3.setInputCloud(cloud_with_normals);
    gp3.setSearchMethod(tree2);
    gp3.reconstruct(triangles);

    // Additional vertex information
    vector<int> parts = gp3.getPartIDs();
    vector<int> states = gp3.getPointStates();

    pcl::io::saveVTKFile("mesh.vtk", triangles);
    pcl::io::saveOBJFile("pointcloud_obj.obj", triangles);
    // Finish
    cout << triangles.polygons.size() << endl;
    return triangles;
}

