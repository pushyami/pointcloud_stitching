cmake_minimum_required (VERSION 2.6 FATAL_ERROR)

project      (pcl-visualizer)
find_package (Qt5Widgets REQUIRED)
find_package (VTK 6.2.0 REQUIRED)
find_package (PCL 1.8.0 REQUIRED)

include_directories (${PCL_INCLUDE_DIRS} ${VTK_INCLUDE_DIRS} ${FLANN_INCLUDE_DIRS})
link_directories    (${PCL_LIBRARY_DIRS})
add_definitions     (${PCL_DEFINITIONS})

set  (project_SOURCES main.cpp pclviewer.cpp Pcl_IO.cpp Pcl_Keypoints.cpp Pcl_Filtering.cpp Pcl_Descriptors.cpp Pcl_Correspondences.cpp Pcl_CorrespondenceFilter.cpp Pcl_Reconstruction.cpp Pcl_Registration.cpp Pcl_Visualization.cpp)
set  (project_HEADERS pclviewer.h PCL_IO.h Pcl_Keypoints.h Pcl_Filtering.h Pcl_Descriptors.h Pcl_Correspondences.h Pcl_CorrespondenceFilter.h Pcl_Reconstruction.h Pcl_Registration.h Pcl_Visualization.h)
set  (project_FORMS   pclviewer.ui)

QT5_WRAP_CPP (project_HEADERS_MOC   ${project_HEADERS})
QT5_WRAP_UI  (project_FORMS_HEADERS ${project_FORMS})


ADD_DEFINITIONS (${QT_DEFINITIONS})

ADD_EXECUTABLE  (pcl_visualizer ${project_SOURCES}
                                ${project_FORMS_HEADERS}
                                ${project_HEADERS_MOC})

TARGET_LINK_LIBRARIES (pcl_visualizer ${PCL_LIBRARIES} ${VTK_LIBRARIES})
qt5_use_modules (pcl_visualizer Widgets)