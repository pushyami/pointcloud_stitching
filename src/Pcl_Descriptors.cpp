#include "Pcl_Descriptors.h"

using namespace pcl;
using namespace std;

void Pcl_Descriptors::initXYZ()
{
    keyPointsXYZ = KeyPointXYZPtr(new PointCloud<PointXYZ>);
    copyPointCloud(*keyPoints, *keyPointsXYZ);

    treeXYZ = KdTreeXYZPtr(new search::KdTree<PointXYZ>);

    cloudXYZ = PointCloudXYZPtr(new PointCloud<PointXYZ>);
    copyPointCloud(*cloudRGB, *cloudXYZ);
}

void Pcl_Descriptors::initRGB()
{
    keyPointsRGB = KeyPointRGBPtr(new PointCloud<PointXYZRGB>);
    copyPointCloud(*keyPoints, *keyPointsRGB);
    treeRGB = KdTreeRGBPtr(new search::KdTree<PointXYZRGB>);
}

// ----------------------------------------------------------------
// -----Calculate surface normals with a search radius-----
// ----------------------------------------------------------------
void Pcl_Descriptors::computeNormals(PointCloudRGBPtr c, KeyPointPtr kp)
{
    cloudRGB = c;
    keyPoints = kp;
    normals = NormalPtr(new PointCloud<Normal>);

    NormalEstimation<PointXYZRGB, Normal> ne;
    search::KdTree<PointXYZRGB>::Ptr tree(new search::KdTree<PointXYZRGB>());

    ne.setInputCloud(cloudRGB);
    ne.setSearchMethod(tree);
    ne.setRadiusSearch(normalEstRad);
    ne.compute(*normals);
	cout << "normal radius" << normalEstRad << endl;

}

//Implementation for extracting point feature histograms with RGB data descriptors
void Pcl_Descriptors::extractPFHRGBDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, PointCloud<PFHRGBSignature250>::Ptr descriptors)
{
    computeNormals( c,kp);
    initRGB();
    PFHRGBEstimation<PointXYZRGB, Normal, PFHRGBSignature250> est;
    est.setSearchMethod(treeRGB);
    est.setRadiusSearch(searchRadius); //0.07
    est.setSearchSurface(cloudRGB);
    est.setInputNormals(normals);
    est.setInputCloud(keyPointsRGB);
    est.compute(*descriptors);
}



void Pcl_Descriptors::extractSHOTCOLORDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, PointCloud<SHOT1344>::Ptr descriptors)
{
    computeNormals( c,kp);
    initRGB();
    SHOTColorEstimation<PointXYZRGB, Normal, SHOT1344> est;
    est.setSearchMethod(treeRGB);
    est.setRadiusSearch(searchRadius);
    est.setSearchSurface(cloudRGB);
    est.setInputNormals(normals);
    est.setInputCloud(keyPointsRGB);
    est.compute(*descriptors);
}

void Pcl_Descriptors::extractSHOTCOLOROMPDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, PointCloud<SHOT1344>::Ptr descriptors)
{
    computeNormals( c,kp);
    initRGB();
    SHOTColorEstimationOMP<PointXYZRGB, Normal, SHOT1344> est;
    est.setSearchMethod(treeRGB);
    est.setRadiusSearch(searchRadius);
    est.setSearchSurface(cloudRGB);
    est.setInputNormals(normals);
    est.setInputCloud(keyPointsRGB);
    est.compute(*descriptors);
	cout << "feature search radius " << searchRadius << endl;
}


void Pcl_Descriptors::extractFPFHDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, PointCloud<FPFHSignature33>::Ptr descriptors)
{
    computeNormals( c,kp);
    initXYZ();
    FPFHEstimation<PointXYZ, Normal, FPFHSignature33> est;
    est.setSearchMethod(treeXYZ);
    est.setRadiusSearch(searchRadius);
    est.setSearchSurface(cloudXYZ);
    est.setInputNormals(normals);
    est.setInputCloud(keyPointsXYZ);
    est.compute(*descriptors);
}


