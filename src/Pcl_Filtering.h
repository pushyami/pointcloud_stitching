#ifndef PCL_FILTERING_H
#define PCL_FILTERING_H

#pragma once

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/passthrough.h>
#include <iostream>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/surface/gp3.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>

class Pcl_Filtering
{

public:
    int passThroughFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut);
    int radialOutlierFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut);
    int voxelGridFilter(const pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudIn, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudOut);

    //pass through filter options
    std::string axis;
    float min;
    float max;

    //voxel grid filter options
    float x;
    float y;
    float z;

    //radial outlier filter options
    float radius;
    int neighbours;

};

#endif // PCL_FILTERING_H
