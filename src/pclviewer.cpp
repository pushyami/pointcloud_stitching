#include "pclviewer.h"
#include "../build/ui_pclviewer.h"

using namespace pcl;

PCLViewer::PCLViewer (QWidget *parent) :  QMainWindow (parent), ui (new Ui::PCLViewer)
{
  ui->setupUi (this);
  this->setWindowTitle ("PCL viewer");


  // Setup the cloud pointer
  cloud.reset (new PointCloudT);
  // The number of points in the cloud
  cloud->points.resize (200);

  // The default color
  red   = 128;
  green = 128;
  blue  = 128;

  // Fill the cloud with some points
  for (size_t i = 0; i < cloud->points.size (); ++i)
  {
    cloud->points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud->points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud->points[i].z = 1024 * rand () / (RAND_MAX + 1.0f);

    cloud->points[i].r = red;
    cloud->points[i].g = green;
    cloud->points[i].b = blue;
  }



  // Set up the QVTK window
  viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
  ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
  viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());
  ui->qvtkWidget->update ();

  // Connect "random" button and the function
  connect (ui->pushButton_random,  SIGNAL (clicked ()), this, SLOT (randomButtonPressed ()));
  // connect the load file button and the function
  connect (ui->loadFilesButton, SIGNAL (clicked ()),this, SLOT (loadFilesButtonPressed()));

  // connect the filters checkboxes to the corresponding function
  connect (ui->passThruFilter_chk, SIGNAL (stateChanged(int)), this, SLOT (passThruFilterChkBx_StateChanged(int)));
  connect (ui->voxelGridFilter_chk, SIGNAL (stateChanged(int)), this, SLOT (voxelGridFilterChkBx_StateChanged(int)));
  connect (ui->radOutlierFilter_chk, SIGNAL (stateChanged(int)), this, SLOT (radOutlierFilterChkBx_StateChanged(int)));

  // connect the feature descriptor selection and function
  connect (ui->FPFH_Radio, SIGNAL(clicked(bool)), this, SLOT(featureDescriptor_Selected(bool)));
  connect (ui->PFHRGB_Radio, SIGNAL(clicked(bool)), this, SLOT(featureDescriptor_Selected(bool)));
  connect (ui->SHOTColor_radio, SIGNAL(clicked(bool)), this, SLOT(featureDescriptor_Selected(bool)));
  connect (ui->SHOTColorOMP_Radio, SIGNAL(clicked(bool)), this, SLOT(featureDescriptor_Selected(bool)));

  // Connect the Correspondence estimator selection and function
  connect(ui->corrsEstManual_Radio, SIGNAL(clicked(bool)), this, SLOT (correspondenceEst_selected(bool)));
  connect(ui->corrsEstReciprocal_Radio, SIGNAL(clicked(bool)), this, SLOT (correspondenceEst_selected(bool)));
  
  // Connect the Correspondence Rejector selection and function
  connect(ui->corrsRejRANSAC_Radio, SIGNAL(clicked(bool)), this, SLOT (correspondenceRejector_selected(bool)));
  connect(ui->corrsRejMedianDist_Radio, SIGNAL(clicked(bool)), this, SLOT (correspondenceRejector_selected(bool)));
  connect(ui->corrsRejDist_Radio, SIGNAL(clicked(bool)), this, SLOT (correspondenceRejector_selected(bool)));

  // Connect the transform estimator selection and function
  connect(ui->transformSVD_Radio, SIGNAL(clicked(bool)), this, SLOT (transformEstimator_Selected(bool)));
  connect(ui->transformLM_Radio, SIGNAL(clicked(bool)), this, SLOT (transformEstimator_Selected(bool)));

  //Connect the exportType selection and function
  connect (ui->csv_Radio, SIGNAL (clicked(bool)), this, SLOT (exportType_Selected(bool)));
  connect (ui->obj_Radio, SIGNAL (clicked(bool)), this, SLOT (exportType_Selected(bool)));

  // Connect point size slider
  connect (ui->horizontalSlider_p, SIGNAL (valueChanged (int)), this, SLOT (pSliderValueChanged (int)));

  //Connect the stitch Button to its function
  connect (ui->Stitch_Button, SIGNAL (clicked()), this, SLOT (stitchButtonClicked()));

  viewer->addPointCloud (cloud, "cloud");
  pSliderValueChanged (2);
  viewer->resetCamera ();
  ui->qvtkWidget->update ();
}

void
PCLViewer::randomButtonPressed ()
{
  printf ("Random button was pressed\n");

  // Set the new color
  for (size_t i = 0; i < cloud->size(); i++)
  {
    cloud->points[i].r = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
    cloud->points[i].g = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
    cloud->points[i].b = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
  }

  viewer->updatePointCloud (cloud, "cloud");
  ui->qvtkWidget->update ();
}

void
PCLViewer::loadFilesButtonPressed()
{
    clouds.clear();

    printf("Load files button was pressed\n");
    QStringList files = QFileDialog::getOpenFileNames(this,tr ("Select point clouds "),"c:\\", "Csv (*.csv)" );

    cout<< files.at(0).toLocal8Bit().constData()<<endl;

    if( files.isEmpty())
        return;

    QStringList::const_iterator constIterator;
    PointCloudT::Ptr temp(new PointCloudT);

    for (constIterator = files.constBegin(); constIterator != files.constEnd(); ++constIterator){

        PointCloudT v;
        *temp = v;
        io.read_csv_file((*constIterator).toLocal8Bit().constData(), temp);
         cout<< (*constIterator).toLocal8Bit().constData()<<endl;
		 PointCloudT::Ptr t = temp->makeShared();
		 clouds.push_back(t);

    }

    registrationObj.allClouds = clouds;
    cout<<"no of clouds"<< clouds.size()<<endl;

}

void PCLViewer::passThruFilterChkBx_StateChanged(int state)
{
   registrationObj.options.PASSTHRU_FILTER_ENABLED = state ? 2 : 0 ;
   cout<<"pass through filter"<<endl;
}


void PCLViewer::voxelGridFilterChkBx_StateChanged(int state)
{
     registrationObj.options.VOXEL_FILTER_ENABLED = state ? 2 : 0 ;
     cout<<"voxel grid filter"<<endl;
}

void PCLViewer::radOutlierFilterChkBx_StateChanged(int state)
{
     registrationObj.options.RADOUTLIER_FILTER_ENABLED = state ? 2 : 0 ;
     cout<<"Radial Outlier filter"<<endl;
}

void PCLViewer::featureDescriptor_Selected(bool select)
{
   if(ui->FPFH_Radio->isChecked())
   {
      registrationObj.options.descriptorType = FPFH;

   }


   if (ui->SHOTColor_radio->isChecked())
   {
      registrationObj.options.descriptorType = SHOTCOLOR;

   }
       

   if (ui->SHOTColorOMP_Radio->isChecked())
   {
       registrationObj.options.descriptorType = SHOTCOLOR_OMP;

   }
       

   if (ui->PFHRGB_Radio->isChecked())
   {
       registrationObj.options.descriptorType = PFHRGB;
       //corrsObj = new Pcl_Correspondences<PFHRGBSignature250>();
   }
     

   cout<< registrationObj.options.descriptorType<<endl;
}

void PCLViewer::correspondenceEst_selected(bool select)
{
  if(ui->corrsEstManual_Radio->isChecked())
      registrationObj.options.corrsEstType = MANUAL;

  if(ui->corrsEstReciprocal_Radio->isChecked())
      registrationObj.options.corrsEstType = RECIPROCAL;

  cout<<registrationObj.options.corrsEstType<<endl;
}

void PCLViewer::correspondenceRejector_selected(bool select)
{
    if(ui->corrsRejRANSAC_Radio->isChecked())
        registrationObj.options.corrsRejectorType = RANSAC;

    if(ui->corrsRejMedianDist_Radio->isChecked())
        registrationObj.options.corrsRejectorType = MEDIAN;

    if(ui->corrsRejDist_Radio->isChecked())
        registrationObj.options.corrsRejectorType = DISTANCETHRESHOLD;

    cout<<registrationObj.options.corrsRejectorType<<endl;
}

void PCLViewer::transformEstimator_Selected(bool select)
{
    if(ui->transformSVD_Radio->isChecked())
        registrationObj.options.transformEstType = SVD;

    if(ui->transformLM_Radio->isChecked())
       registrationObj.options.transformEstType = LM;

    cout<<registrationObj.options.transformEstType<<endl;
}

void PCLViewer::exportType_Selected(bool select)
{
    if(ui->csv_Radio->isChecked())
    {
        registrationObj.options.exportFormat = CSV;
    }
    if(ui->obj_Radio->isChecked())
    {
        registrationObj.options.exportFormat = OBJ;
    }

}

void
PCLViewer::pSliderValueChanged (int value)
{
  viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, value, "cloud");
  ui->qvtkWidget->update ();
}

void
PCLViewer::stitchButtonClicked()
{
   

	switch (ui->axisComboBox->currentIndex())
		  {
		     case 0:
                registrationObj.filterObj.axis ="x";
		        cout<<"axis x"<<endl;
		        break;
		     case 1:
                registrationObj.filterObj.axis ="y";
		        cout<<"axis y"<<endl;
		        break;
		     case 2:
                registrationObj.filterObj.axis ="z";
		        cout<<"axis z"<<endl;
		        break;

		  }

    registrationObj.filterObj.max = ui->rangeMax->toPlainText().toFloat();
    registrationObj.filterObj.min = ui->rangeMin->toPlainText().toFloat();

    registrationObj.filterObj.x = ui->voxelCubeX->toPlainText().toFloat();
    registrationObj.filterObj.y = ui->voxelCubeY->toPlainText().toFloat();
    registrationObj.filterObj.z = ui->voxelCubeZ->toPlainText().toFloat();

    registrationObj.filterObj.radius = ui->OutlierRadius->toPlainText().toFloat();
    registrationObj.filterObj.neighbours = ui->spinBox->cleanText().toInt();

    cout<<ui->OutlierRadius->toPlainText().toFloat()<<endl;

    registrationObj.options.keyPointType = SIFT;

    registrationObj.keyPoints.minScale = ui->siftMinScale_SpnBx->cleanText().toFloat();
    registrationObj.keyPoints.noOctaves = ui->numOctaves_SpnBx->cleanText().toFloat();
    registrationObj.keyPoints.scalesPerOctave = ui->numScales_SpnBx->cleanText().toFloat();
    registrationObj.keyPoints.contrast = ui->siftMinContrast_SpnBx->cleanText().toFloat();




    registrationObj.descriptorObj.normalEstRad = ui->computeNormalsRad_SpnBx->cleanText().toFloat();
    registrationObj.descriptorObj.searchRadius = ui->featureSearchRad_SpnBx->cleanText().toFloat();

    registrationObj.stitchPointCloudData();

    viewer->removePointCloud("cloud");
    viewer->addPointCloud(registrationObj.stitchedCloud, "cloud1");
    viewer->resetCamera ();
    ui->qvtkWidget->update ();

}

PCLViewer::~PCLViewer ()
{
  delete ui;
}

//void PCLViewer::passThruFilterAxis_IndexChanged(int index)
//{
//  switch(index)
//  {
//     case 0:
//        filterObj.axis ="x";
//        cout<<"axis x"<<endl;
//        break;
//     case 1:
//        filterObj.axis ="y";
//        cout<<"axis y"<<endl;
//        break;
//     case 2:
//        filterObj.axis ="z";
//        cout<<"axis z"<<endl;
//        break;

//  }
//}
