#ifndef PCL_KEYPOINTS_H
#define PCL_KEYPOINTS_H

#pragma once

#include <pcl/PCLPointCloud2.h>
#include <pcl/keypoints/keypoint.h>
#include <pcl/keypoints/sift_keypoint.h>
#include <iostream>


class Pcl_Keypoints
{
private:
	typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudPtr;
	typedef pcl::PointCloud<pcl::PointWithScale>::Ptr KeyPointsPtr;

	PointCloudPtr cloud;
	KeyPointsPtr keyPoints;

public:
    void sift(PointCloudPtr cloud);
    KeyPointsPtr getKeypointPtr()
    {
        return keyPoints;
    }

    float minScale;
    float noOctaves;
    float scalesPerOctave;
    float contrast;

};

#endif // PCL_KEYPOINTS_H
