#ifndef PCL_DESCRIPTORS_H
#define PCL_DESCRIPTORS_H

#pragma once

#include <pcl/keypoints/keypoint.h>
#include <pcl/features/shot.h>
#include <pcl/features/shot_omp.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/pfhrgb.h>
#include <pcl/features/normal_3d.h>


class Pcl_Descriptors
{
private:
    typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudRGBPtr;
    typedef pcl::PointCloud<pcl::PointXYZ>::Ptr PointCloudXYZPtr;
    typedef pcl::PointCloud<pcl::PointWithScale>::Ptr KeyPointPtr;
    typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr KeyPointRGBPtr;
    typedef pcl::PointCloud<pcl::PointXYZ>::Ptr KeyPointXYZPtr;
    typedef pcl::PointCloud<pcl::Normal>::Ptr NormalPtr;
    typedef pcl::search::KdTree<pcl::PointXYZRGB>::Ptr  KdTreeRGBPtr;
    typedef pcl::search::KdTree<pcl::PointXYZ>::Ptr  KdTreeXYZPtr;

    PointCloudRGBPtr cloudRGB;
    KeyPointRGBPtr keyPointsRGB;
    KdTreeRGBPtr treeRGB;

    KeyPointPtr keyPoints;
    NormalPtr normals;

    PointCloudXYZPtr cloudXYZ;
    KeyPointXYZPtr keyPointsXYZ;
    KdTreeXYZPtr treeXYZ;

    void *estimator;

    void initRGB();
    void initXYZ();

public:
    void computeNormals(PointCloudRGBPtr c, KeyPointPtr kp);

    void extractPFHRGBDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, pcl::PointCloud<pcl::PFHRGBSignature250>::Ptr descriptors);
    void extractSHOTCOLORDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, pcl::PointCloud<pcl::SHOT1344>::Ptr descriptors);
    void extractSHOTCOLOROMPDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, pcl::PointCloud<pcl::SHOT1344>::Ptr descriptors);
    void extractFPFHDescriptor(PointCloudRGBPtr c, KeyPointPtr kp, pcl::PointCloud<pcl::FPFHSignature33>::Ptr descriptors);

    float normalEstRad;
    float searchRadius;
};

#endif // PCL_DESCRIPTORS_H
