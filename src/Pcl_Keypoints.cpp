#include "Pcl_Keypoints.h"

using namespace pcl;
using namespace std;


// Finding SIFT key points for the input cloud

void Pcl_Keypoints::sift(PointCloudPtr cloud)
{
	SIFTKeypoint<PointXYZRGB, PointWithScale> sift;
	keyPoints = KeyPointsPtr(new PointCloud<PointWithScale>);

	search::KdTree<PointXYZRGB>::Ptr tree(new pcl::search::KdTree<PointXYZRGB>());
	sift.setSearchMethod(tree);
	sift.setKSearch(100);
	sift.setScales(minScale, noOctaves, scalesPerOctave);
	sift.setMinimumContrast(contrast);
	sift.setInputCloud(cloud);
	sift.compute(*keyPoints);
}

