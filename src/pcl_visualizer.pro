#-------------------------------------------------
#
# Project created by QtCreator 2014-05-01T14:24:33
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pcl_visualizer
TEMPLATE = app


SOURCES += main.cpp\
        pclviewer.cpp \
    Pcl_IO.cpp \
    Pcl_Keypoints.cpp \
    Pcl_Filtering.cpp \
    Pcl_Descriptors.cpp \
    Pcl_Correspondences.cpp \
    pcl_CorrespondenceFilter.cpp \
    Pcl_Reconstruction.cpp \
    Pcl_Registration.cpp \
    Pcl_Visualization.cpp

HEADERS  += pclviewer.h \
    Pcl_IO.h \
    Pcl_Keypoints.h \
    Pcl_Filtering.h \
    Pcl_Descriptors.h \
    Pcl_Correspondences.h \
    Pcl_CorrespondenceFilter.h \
    Pcl_Reconstruction.h \
    Pcl_Registration.h \
    Pcl_Visualization.h

FORMS    += pclviewer.ui