#include "Pcl_Registration.h"

using namespace pcl;
using namespace std;

Pcl_Registration::Pcl_Registration(PointCloudPtr s, PointCloudPtr d) : srcPC{ s }, destPC{ d }
{
}

Pcl_Registration::Pcl_Registration()
{
}


Pcl_Registration::~Pcl_Registration()
{
}

void Pcl_Registration::registrationPipelineExec()
{
    //Preprocessing Filtering Data and cleanup
    if(options.PASSTHRU_FILTER_ENABLED){
        filterObj.passThroughFilter(srcPC,srcPC);
    
    }
    if(options.VOXEL_FILTER_ENABLED)
    {
        filterObj.voxelGridFilter(srcPC, srcPC);
       

    }
    if(options.RADOUTLIER_FILTER_ENABLED)
    {
        filterObj.radialOutlierFilter(srcPC, srcPC);
     
    }

    //finding key points
    if(options.keyPointType == SIFT)
    {
        keyPoints.sift(srcPC);
        srcKP = keyPoints.getKeypointPtr();
        cout << "Key points in image 1: " << srcKP->size() << endl;
       
    }

    //extracting Descrptors and finding correspondences

    std::vector<int> corr_Src2Tgt, corr_Tgt2Src;

    switch(options.descriptorType)
    {
	case FPFH:
	{
		PointCloud<FPFHSignature33>::Ptr descriptorsSrc(new PointCloud<FPFHSignature33>());
		PointCloud<FPFHSignature33>::Ptr descriptorsTgt(new PointCloud<FPFHSignature33>());
        descriptorObj.extractFPFHDescriptor(srcPC, srcKP,descriptorsSrc);
        descriptorObj.extractFPFHDescriptor(destPC, destKP,descriptorsTgt);

		Pcl_Correspondences<FPFHSignature33> correspondences;
		correspondences.findCorrespondenceDistance(descriptorsSrc, descriptorsTgt);
		corr_Src2Tgt = correspondences.getCorrespondenceDistance();
		correspondences.findCorrespondenceDistance(descriptorsTgt, descriptorsSrc);
		corr_Tgt2Src = correspondences.getCorrespondenceDistance();
	}
            break;

	case SHOTCOLOR:
	{
		PointCloud<SHOT1344>::Ptr descriptorsSrc(new PointCloud<SHOT1344>());
		PointCloud<SHOT1344>::Ptr descriptorsTgt(new PointCloud<SHOT1344>());
        descriptorObj.extractSHOTCOLORDescriptor(srcPC, srcKP,descriptorsSrc);
        descriptorObj.extractSHOTCOLORDescriptor(destPC, destKP,descriptorsTgt);

		Pcl_Correspondences<SHOT1344> correspondences;
		correspondences.findCorrespondenceDistance_SHOT(descriptorsSrc, descriptorsTgt);
		corr_Src2Tgt = correspondences.getCorrespondenceDistance();
		correspondences.findCorrespondenceDistance_SHOT(descriptorsTgt, descriptorsSrc);
		corr_Tgt2Src = correspondences.getCorrespondenceDistance();
	}
            break;

	case SHOTCOLOR_OMP:
	{
		PointCloud<SHOT1344>::Ptr descriptorsSrc(new PointCloud<SHOT1344>());
		PointCloud<SHOT1344>::Ptr descriptorsTgt(new PointCloud<SHOT1344>());
        descriptorObj.extractSHOTCOLOROMPDescriptor(srcPC, srcKP,descriptorsSrc);
        descriptorObj.extractSHOTCOLOROMPDescriptor(destPC, destKP,descriptorsTgt);

		Pcl_Correspondences<SHOT1344> correspondences;
		correspondences.findCorrespondenceDistance_SHOT(descriptorsSrc, descriptorsTgt);
		corr_Src2Tgt = correspondences.getCorrespondenceDistance();
		correspondences.findCorrespondenceDistance_SHOT(descriptorsTgt, descriptorsSrc);
		corr_Tgt2Src = correspondences.getCorrespondenceDistance();
	}
            break;

	case PFHRGB:
	{
		PointCloud<PFHRGBSignature250>::Ptr descriptorsSrc(new PointCloud<PFHRGBSignature250>());
		PointCloud<PFHRGBSignature250>::Ptr descriptorsTgt(new PointCloud<PFHRGBSignature250>());
        descriptorObj.extractPFHRGBDescriptor(srcPC, srcKP,descriptorsSrc);
        descriptorObj.extractPFHRGBDescriptor(destPC, destKP,descriptorsTgt);

		Pcl_Correspondences<PFHRGBSignature250> correspondences;
		correspondences.findCorrespondenceDistance(descriptorsSrc, descriptorsTgt);
		corr_Src2Tgt = correspondences.getCorrespondenceDistance();
		correspondences.findCorrespondenceDistance(descriptorsTgt, descriptorsSrc);
		corr_Tgt2Src = correspondences.getCorrespondenceDistance();
	}
            break;
	default:
	{
		PointCloud<SHOT1344>::Ptr descriptorsSrc(new PointCloud<SHOT1344>());
		PointCloud<SHOT1344>::Ptr descriptorsTgt(new PointCloud<SHOT1344>());
        descriptorObj.extractSHOTCOLOROMPDescriptor(srcPC, srcKP,descriptorsSrc);
        descriptorObj.extractSHOTCOLOROMPDescriptor(destPC, destKP,descriptorsTgt);

		Pcl_Correspondences<SHOT1344> correspondences;
		correspondences.findCorrespondenceDistance_SHOT(descriptorsSrc, descriptorsTgt);
		corr_Src2Tgt = correspondences.getCorrespondenceDistance();
		correspondences.findCorrespondenceDistance_SHOT(descriptorsTgt, descriptorsSrc);
		corr_Tgt2Src = correspondences.getCorrespondenceDistance();
	}
            break;
       
    }
    cout << "Number of Correspondences from Source to Target: " << corr_Src2Tgt.size() << endl;
    cout << "Number of Correspondences from Target to Source: " << corr_Tgt2Src.size() << endl;

    // Correspondence Rejection
    int type =0;
    switch(options.corrsRejectorType)
    {
        case RANSAC:
            type = 0;
            break;
        case MEDIAN:
            type = 1;
            break;
        case DISTANCETHRESHOLD:
            type = 2;
            break;
    }

    corrsRejectorObj.threshold = 0.05;
    corrsRejectorObj.filterCorrespondences(corr_Src2Tgt, corr_Tgt2Src, srcKP, destKP, type);
    corres = corrsRejectorObj.getFilteredCorrespondence();

    // Transformation Estimation
    switch(options.transformEstType)
    {
	case SVD:
	{
		registration::TransformationEstimationSVD<PointWithScale, PointWithScale> svd;
		svd.estimateRigidTransformation(*srcKP, *destKP, *corres, transformation);
	}
        break;
    case LM:
        cout<<"not implemented yet"<<endl;
        break;
	default:
	{
		registration::TransformationEstimationSVD<PointWithScale, PointWithScale> svd;
		svd.estimateRigidTransformation(*srcKP, *destKP, *corres, transformation);
	}
    }

    mainTransform = mainTransform * transformation ;
	transformPointCloud(*srcPC, *cloudTemp, mainTransform);
    *stitchedCloud += *cloudTemp;

    //clenaing up the variables
    destPC->clear();
    *destPC = *srcPC;

    destKP->clear();
    *destKP = * srcKP;

     srcPC->clear();
     srcKP->clear();
}


void Pcl_Registration::stitchPointCloudData()
{
    srcPC = PointCloudPtr(new PointCloud<PointXYZRGB>);
    destPC = PointCloudPtr(new PointCloud<PointXYZRGB>);
    cloudTemp = PointCloudPtr(new PointCloud<PointXYZRGB>);
    stitchedCloud = PointCloudPtr(new PointCloud<PointXYZRGB>);

	*destPC = * allClouds.back();
	allClouds.pop_back();

	if (options.PASSTHRU_FILTER_ENABLED){
		filterObj.passThroughFilter(destPC, destPC);
	}
	if (options.VOXEL_FILTER_ENABLED)
	{
		filterObj.voxelGridFilter(destPC, destPC);

	}
	if (options.RADOUTLIER_FILTER_ENABLED)
	{
		filterObj.radialOutlierFilter(destPC, destPC);
	}

	*stitchedCloud += *destPC;

	//finding key points
	if (options.keyPointType == SIFT)
	{
		keyPoints.sift(destPC);
		destKP = keyPoints.getKeypointPtr();
		cout << "Key points in image 2: " << destKP->size() << endl;
	}

	while (!allClouds.empty())
	{
		*srcPC = *allClouds.back();
		allClouds.pop_back();
		registrationPipelineExec();
	}

	filterObj.x = 0.01;
	filterObj.y = 0.01;
	filterObj.z = 0.01;

	filterObj.radius = 0.02;
	filterObj.neighbours = 10;

	filterObj.voxelGridFilter(stitchedCloud, stitchedCloud);
    //filterObj.radialOutlierFilter(stitchedCloud, stitchedCloud);
	
}



void Pcl_Registration::calcTransformation()
{

//    //Step 1. Calculate keypoints for src and dest point clouds
//    Pcl_Keypoints keypoints;

//    float min_scale, no_octaves, scalesPerOctave, min_cont;
//    cout << "enter the min scale for SIFT:" << endl;
//    cin >> min_scale;
//    cout << "enter the no of octaves for SIFT:" << endl;
//    cin >> no_octaves;
//    cout << "enter the no of scales per octave for SIFT:" << endl;
//    cin >> scalesPerOctave;
//    cout << "enter the min contrast for SIFT:" << endl;
//    cin >> min_cont;

//    keypoints.sift(srcPC, min_scale, no_octaves, scalesPerOctave, min_cont);
//    srcKP = keypoints.getKeypointPtr();
//    cout << "Key points in image 1: " << srcKP->size() << endl;

//    keypoints.sift(destPC, min_scale, no_octaves, scalesPerOctave, min_cont);
//    destKP = keypoints.getKeypointPtr();
//    cout << "Key points in image 2: " << destKP->size() << endl;


//    // Step 2. Calculate Feature Descriptors
//    Pcl_Descriptors descriptors;
//    PointCloud<PFHRGBSignature250>::Ptr descriptorsSrc(new PointCloud<PFHRGBSignature250>());
//    PointCloud<PFHRGBSignature250>::Ptr descriptorsTgt(new PointCloud<PFHRGBSignature250>());

//    double normalRadius;
//    cout << "Normal radius: " << endl;
//    cin >> normalRadius;
//    descriptors.computeNormals(srcPC, srcKP, normalRadius);
//    descriptors.extractPFHRGBDescriptor(descriptorsSrc);
//    descriptors.computeNormals(destPC, destKP, normalRadius);
//    descriptors.extractPFHRGBDescriptor(descriptorsTgt);


//    // Step 3. Correspondence Estimation
//    Pcl_Correspondences<PFHRGBSignature250> correspondences;
//    correspondences.findCorrespondenceDistance(descriptorsSrc, descriptorsTgt);
//    std::vector<int> corr_Src2Tgt = correspondences.getCorrespondenceDistance();
//    correspondences.findCorrespondenceDistance(descriptorsTgt, descriptorsSrc);
//    std::vector<int> corr_Tgt2Src = correspondences.getCorrespondenceDistance();
//    cout << "Number of Correspondences from Source to Target: " << corr_Src2Tgt.size() << endl;
//    cout << "Number of Correspondences from Target to Source: " << corr_Tgt2Src.size() << endl;

//    // Step 4. Correspondence Rejection
//    Pcl_CorrespondenceFilter correspondenceFilter;
//    correspondenceFilter = Pcl_CorrespondenceFilter();
//    float threshold;
//    cout << "Enter the threshold for correspondence rejection" << endl;
//    cin >> threshold;
//    int type = 1;
//    correspondenceFilter.filterCorrespondences(corr_Src2Tgt, corr_Tgt2Src, srcKP, destKP, threshold, type);
//    corres = correspondenceFilter.getFilteredCorrespondence();


//    // Step 5. Transformation Estimation
//    registration::TransformationEstimationSVD<PointWithScale, PointWithScale> svd;
//    svd.estimateRigidTransformation(*srcKP, *destKP, *corres, transformation);

}

void Pcl_Registration::visualise()
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
	pcl::visualization::PointCloudColorHandlerRGBField<PointXYZRGB> rgb(stitchedCloud);
	viewer->addPointCloud<pcl::PointXYZRGB>(stitchedCloud, rgb, "cloud");

   /* pcl::visualization::PointCloudColorHandlerRGBField<PointXYZRGB> rgb(srcPC);
    pcl::visualization::PointCloudColorHandlerRGBField<PointXYZRGB> rgb2(destPC);
    viewer->addPointCloud<pcl::PointXYZRGB>(srcPC, rgb, "cloud1");
    viewer->addPointCloud<pcl::PointXYZRGB>(destPC, rgb2, "cloud2");

	PointCloud<PointXYZ>::Ptr temp(new PointCloud<PointXYZ>);
	copyPointCloud(*srcKP, *temp);
	PointCloud<PointXYZ>::Ptr temp2(new PointCloud<PointXYZ>);
	copyPointCloud(*destKP, *temp2);
    viewer->addPointCloud<PointXYZ>(temp,"key1");
    viewer->addPointCloud<PointXYZ>(temp2, "key2");

	
    viewer->addCorrespondences<PointWithScale>(srcKP, destKP, *corres, "correspndences");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "key1");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 6, "key2");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud1");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "cloud2");*/

    viewer->initCameraParameters();

    while (!viewer->wasStopped())
    {
        viewer->spinOnce(10000);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
}

