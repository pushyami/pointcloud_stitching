#ifndef PCLVIEWER_H
#define PCLVIEWER_H

#include <iostream>

// Qt
#include <QMainWindow>
#include <QFileDialog>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/fpfh.h>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

//Other includes
#include "Pcl_IO.h"
#include "Pcl_Filtering.h"
#include "Pcl_Registration.h"
#include "Pcl_Descriptors.h"
#include "Pcl_Correspondences.h"
#include "Pcl_CorrespondenceFilter.h"


typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

namespace Ui
{
  class PCLViewer;
}

class PCLViewer : public QMainWindow
{
  Q_OBJECT

public:
  explicit PCLViewer (QWidget *parent = 0);
  ~PCLViewer ();

public slots:
  void
  randomButtonPressed ();

  void
  loadFilesButtonPressed ();

  void
  passThruFilterChkBx_StateChanged(int state);

  void
  voxelGridFilterChkBx_StateChanged(int state);

  void
  radOutlierFilterChkBx_StateChanged(int state);

 
  void
  featureDescriptor_Selected(bool select);

  void
  correspondenceEst_selected(bool select);

  void
  correspondenceRejector_selected(bool select);

  void
  transformEstimator_Selected(bool select);

  void
  exportType_Selected(bool select);

  void
  pSliderValueChanged (int value);

  void
  stitchButtonClicked();


protected:
  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
  PointCloudT::Ptr cloud;
  std::vector<PointCloudT::Ptr> clouds;
  Pcl_IO io;
  Pcl_Registration registrationObj;

  unsigned int red;
  unsigned int green;
  unsigned int blue;


private:
  Ui::PCLViewer *ui;
  void passThruFilterAxis_IndexChanged(int index);


};

#endif // PCLVIEWER_H
