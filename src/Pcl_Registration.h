#ifndef PCL_REGISTRATION_H
#define PCL_REGISTRATION_H

#pragma once
#include "Pcl_Keypoints.h"
#include "Pcl_Descriptors.h"
#include "Pcl_Correspondences.h"
#include "Pcl_CorrespondenceFilter.h"
#include "Pcl_Visualization.h"
#include "Pcl_Filtering.h"

enum KeyPoint_Type{
    SIFT
};

enum Descriptor_Type {

    FPFH,
    PFHRGB,
    SHOTCOLOR,
    SHOTCOLOR_OMP
};

enum CorrsEstimator_Type {

    MANUAL,
    RECIPROCAL
};

enum CorrsRejector_Type{
    RANSAC,
    MEDIAN,
    DISTANCETHRESHOLD
};

enum TransformEst_Type{
    SVD,
    LM
};

enum ExportFormat{
    CSV,
    OBJ
};

class RegistrationOptions
{

public:
    bool PASSTHRU_FILTER_ENABLED = false;
    bool VOXEL_FILTER_ENABLED = false;
    bool RADOUTLIER_FILTER_ENABLED = false;
    KeyPoint_Type keyPointType = SIFT;
    Descriptor_Type descriptorType = SHOTCOLOR_OMP;
    CorrsEstimator_Type corrsEstType = MANUAL;
    CorrsRejector_Type corrsRejectorType = RANSAC;
    TransformEst_Type transformEstType = SVD;
    bool runICP = false;
    ExportFormat exportFormat = CSV;


};

class Pcl_Registration
{
private:
    typedef pcl::PointCloud<pcl::PointXYZRGB>::Ptr PointCloudPtr;
    typedef pcl::PointCloud<pcl::PointWithScale>::Ptr KeyPointPtr;

    PointCloudPtr srcPC, destPC,cloudTemp;
    KeyPointPtr srcKP, destKP;
    pcl::CorrespondencesPtr corres;
    Eigen::Matrix4f transformation;

public:
    RegistrationOptions options;
    Eigen::Matrix4f mainTransform = Eigen::Matrix4f::Identity();
    std::vector<PointCloudPtr> allClouds;
    PointCloudPtr stitchedCloud;
    Pcl_Registration(PointCloudPtr, PointCloudPtr);
    Pcl_Registration();
    ~Pcl_Registration();
    void registrationPipelineExec();
    void stitchPointCloudData();
    void calcTransformation();
    void visualise();

    Eigen::Matrix4f getTransformation()
    {
        return transformation;
    }

    Pcl_Filtering filterObj;
    Pcl_Descriptors descriptorObj;
    CorrespondencesBase* corrsObj;
    Pcl_CorrespondenceFilter corrsRejectorObj;
    Pcl_Keypoints keyPoints;

};

#endif // PCL_REGISTRATION_H
