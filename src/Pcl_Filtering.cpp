#include "Pcl_Filtering.h"

using namespace pcl;
using namespace boost;
using namespace std;

//Pass through Filtering//
int Pcl_Filtering::passThroughFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut){

    PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);

    cout << "number of points before filtering" << cloudIn->points.size() << endl;

    //pointcloud filtering using passthrough filter

    PassThrough<PointXYZRGB> filter;
    filter.setInputCloud(cloudIn);
    filter.setFilterFieldName(axis);
    filter.setFilterLimits(min, max);  //-7.1, -5.6
    filter.filter(*cloud_filtered);

    *cloudOut = *cloud_filtered;
    cout << "number of points after filtering" << cloudOut->points.size() << endl;

    return 0;
}

//Radial Outlier filtering//
int Pcl_Filtering::radialOutlierFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut){

    PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);


    //pointcloud filtering using radial outlier removal
    RadiusOutlierRemoval< PointXYZRGB> rad_outlier_filter;
    rad_outlier_filter.setInputCloud(cloudIn);
    rad_outlier_filter.setRadiusSearch(radius); //0.01
    rad_outlier_filter.setMinNeighborsInRadius(neighbours); // 4
    rad_outlier_filter.filter(*cloud_filtered);

    *cloudOut = *cloud_filtered;

    cout<<"radius"<<radius<<" neighbours"<<neighbours<<endl;
    cout << "number of points after filtering" << cloudOut->points.size() << endl;

    return 0;
}

// voxel grid filtering//
int Pcl_Filtering::voxelGridFilter(const PointCloud<PointXYZRGB>::Ptr cloudIn, PointCloud<PointXYZRGB>::Ptr cloudOut){

    PointCloud<PointXYZRGB>::Ptr cloud_filtered(new PointCloud<PointXYZRGB>);
    PCLPointCloud2::Ptr cloudIn2(new PCLPointCloud2());
    PCLPointCloud2::Ptr cloud_filtered2(new PCLPointCloud2());

    cout << "number of points before filtering" << cloudIn->points.size() << endl;

    //convert the point cloud into PointCloud2
    toPCLPointCloud2(*cloudIn, *cloudIn2);

    // pass the pointcloud into a voxelgrid filter
    VoxelGrid<PCLPointCloud2> filter;
    filter.setInputCloud(cloudIn2);
    filter.setLeafSize(x, y, z); //0.025, 0.025, 0.025
    filter.filter(*cloud_filtered2);

    //convert from pointcloud2 to pointcloud
    fromPCLPointCloud2(*cloud_filtered2, *cloud_filtered);

    *cloudOut = *cloud_filtered;

    cout << "number of points after filtering" << cloudOut->points.size() << endl;

    return 0;
}


