#ifndef PCL_CORRESPONDENCEFILTER_H
#define PCL_CORRESPONDENCEFILTER_H

#pragma once

#include <pcl/registration/correspondence_estimation.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_median_distance.h>
#include <iostream>



class Pcl_CorrespondenceFilter
{
private:
    typedef pcl::PointCloud<pcl::PointWithScale>::Ptr KeyPointPtr;
    pcl::CorrespondencesPtr filteredCorrespondence;

public:
    void filterCorrespondences(const pcl::CorrespondencesPtr &all_correspondences, const KeyPointPtr &keypoints_src, const KeyPointPtr &keypoints_tgt, int type);
    void filterCorrespondences(std::vector<int> &correspondences1, std::vector<int> &correspondences2, KeyPointPtr keyPointsSrc, KeyPointPtr keyPointsDst, int type);

    pcl::CorrespondencesPtr getFilteredCorrespondence()
    {
        return filteredCorrespondence;
    }

    float threshold;
};

#endif // PCL_CORRESPONDENCEFILTER_H
