#ifndef PCL_RECONSTRUCTION_H
#define PCL_RECONSTRUCTION_H

#pragma once
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/surface/gp3.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/obj_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>


class Pcl_Reconstruction
{
public:
    Pcl_Reconstruction();
    ~Pcl_Reconstruction();

    pcl::PolygonMesh greedyTriangulation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloudIn);
};

#endif // PCL_RECONSTRUCTION_H
