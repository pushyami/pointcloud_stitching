#ifndef PCL_CORRESPONDENCES_H
#define PCL_CORRESPONDENCES_H

#pragma once

#include <pcl/keypoints/keypoint.h>
#include <pcl/features/shot.h>
#include <pcl/features/fpfh.h>
#include <pcl/features/pfhrgb.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/correspondence_estimation.h>
#include <pcl/registration/correspondence_rejection_distance.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_median_distance.h>
#include <iostream>




class CorrespondencesBase
{
public:
    CorrespondencesBase()
    {

    }
};



template<class T>
class Pcl_Correspondences : public CorrespondencesBase
{
private:
    pcl::CorrespondencesPtr all_correspondences;
    std::vector<int> correspondence;
    std::vector<float> correspondenceScores;

public:

  
	Pcl_Correspondences() : CorrespondencesBase()
	{

	}

    void findCorrespondenceEstimation(boost::shared_ptr<pcl::PointCloud<T>> srcDesc, boost::shared_ptr<pcl::PointCloud<T>> tgtDesc)
    {
        all_correspondences = pcl::CorrespondencesPtr(new Correspondences);
        pcl::registration::CorrespondenceEstimation <T, T> est;
        est.setInputSource(srcDesc);
        est.setInputTarget(tgtDesc);
        est.determineReciprocalCorrespondences(*all_correspondences);
    }

    void findCorrespondenceDistance(boost::shared_ptr<pcl::PointCloud<T>> srcDesc, boost::shared_ptr<pcl::PointCloud<T>> tgtDescs)
    {
        correspondence.resize(srcDesc->size());
        correspondenceScores.resize(srcDesc->size());

        pcl::KdTreeFLANN<T> tree;
        tree.setInputCloud(tgtDescs);

        const int k = 1;
        std::vector<int> k_indices(k);
        std::vector<float> k_squared_distances(k);

        for (size_t i = 0; i < srcDesc->size(); ++i)
        {
			
			//// NOTE: this statement needs to ne commented out for PFHRGB descriptors and ept for SHOT1344 descriptors
		    tree.nearestKSearch(*srcDesc, i, k, k_indices, k_squared_distances);
            correspondence[i] = k_indices[0];
            correspondenceScores[i] = k_squared_distances[0];
        }
    }

	void findCorrespondenceDistance_SHOT(boost::shared_ptr<pcl::PointCloud<pcl::SHOT1344>> srcDesc, boost::shared_ptr<pcl::PointCloud<pcl::SHOT1344>> tgtDescs)
	{
		correspondence.resize(srcDesc->size());
		correspondenceScores.resize(srcDesc->size());

		pcl::KdTreeFLANN<T> tree;
		tree.setInputCloud(tgtDescs);

		const int k = 1;
		std::vector<int> k_indices(k);
		std::vector<float> k_squared_distances(k);

		for (size_t i = 0; i < srcDesc->size(); ++i)
		{

			//// NOTE: this statement needs to ne commented out for PFHRGB descriptors and ept for SHOT1344 descriptors
			if (pcl_isnan(srcDesc->at(i).descriptor[0]))
					continue;
		
			tree.nearestKSearch(*srcDesc, i, k, k_indices, k_squared_distances);
			correspondence[i] = k_indices[0];
			correspondenceScores[i] = k_squared_distances[0];
		}
	}


    pcl::CorrespondencesPtr getCorrespondence()
    {
        return all_correspondences;
    }

    std::vector<int> getCorrespondenceDistance()
    {
        return correspondence;
    }

    std::vector<float> getCorrespondenceScores()
    {
        return correspondenceScores;
    }
};

#endif // PCL_CORRESPONDENCES_H
